package com.thiede.adam.textwall;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        TextView textView = new TextView(this);

        //general text prop
        int textSize = intent.getIntExtra(MainActivity.TEXT_SIZE, 12);
        textView.setTextSize(textSize);
        textView.setText(message);
        textView.setTextIsSelectable(true);

        //set color
        System.out.println("*********************\n" + Color.parseColor("BLACK"));

        String bgColor = intent.getStringExtra(MainActivity.BG_COLOR);
        String fgColor = intent.getStringExtra(MainActivity.FG_COLOR);

        textView.setTextColor(Color.parseColor(fgColor));
        textView.setBackgroundColor(Color.parseColor(bgColor));
        //textView.setDrawingCacheBackgroundColor(Color.parseColor("BLACK"));

        ViewGroup layout = (ViewGroup) findViewById(R.id.activity_display_message);
        layout.addView(textView);
        //layout.setDrawingCacheBackgroundColor(Color.parseColor(bgColor));
        layout.setBackgroundColor(Color.parseColor(bgColor));
    }
}
