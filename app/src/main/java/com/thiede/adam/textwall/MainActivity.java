package com.thiede.adam.textwall;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.thiede.adam.MESSAGE";
    public static final String TEXT_SIZE = "0";
    public static final String BG_COLOR = "WHITE";
    public static final String FG_COLOR = "BLACK";
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //populate text color spinners
        String[] colors = new String[]{"black", "darkgray", "gray", "lightgray", "white",
                "red", "green", "blue", "yellow", "cyan", "magenta", "aqua", "fuchsia",
                "darkgrey", "grey", "lightgrey", "lime", "maroon", "navy", "olive",
                "purple", "silver", "teal"};
        Spinner fgcSpinner = (Spinner) findViewById(R.id.fgc_spinner);
        Spinner bgcSpinner = (Spinner) findViewById(R.id.bgc_spinner);
        ArrayAdapter<String> colorPutter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, colors);
        fgcSpinner.setAdapter(colorPutter);
        fgcSpinner.setSelection(colors.length - 1);
        bgcSpinner.setAdapter(colorPutter);

        //screen on?
        CheckBox awake = (CheckBox) findViewById(R.id.keepon_checkbox);
        if (awake.isChecked()) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        //DOING THE EXAMPLE TEXT SIZE THING!
        final TextView sampleSize = (TextView) findViewById(R.id.text_size_example);
        SeekBar textSizer = (SeekBar) findViewById(R.id.size_seek);
        //I have a seekbar now! Let's see what we can do.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            textSizer.setMin(24);
        }
        textSizer.setMax(256);
        //does this make ANY sense??!?!
        System.out.println("\n\n\n\nTHE SIZE OF THE THING\n" + textSizer.getProgress());
        prefs = getPreferences(MODE_PRIVATE);
        //get sizes and set sizes.
        float fs = prefs.getFloat("fontsize", 10);
        textSizer.setProgress((int) fs);
        //sampleSize.setTextSize(textSizer.getProgress());
        sampleSize.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizer.getProgress());

        textSizer.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                prefs = getPreferences(MODE_PRIVATE);
                SharedPreferences.Editor ed = prefs.edit();
                ed.putFloat("fontsize", sampleSize.getTextSize());
                ed.commit();
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                sampleSize.setTextSize(TypedValue.COMPLEX_UNIT_PX, progress);
            }
        });
    }

    /**
     * Called on button press!
     */
    public void sendMessage(View view) {
        //pressing the button!
        //check if fullscreen is specified
        CheckBox fsBox = (CheckBox) findViewById(R.id.fs_checkbox);
        Intent intent = new Intent();
        if (fsBox.isChecked()) {
            intent = new Intent(this, FullscreenActivity.class);
        } else {
            intent = new Intent(this, DisplayMessageActivity.class);
        }
        EditText editText = (EditText) findViewById(R.id.edit_message);
        Spinner bgColorField = (Spinner) findViewById(R.id.bgc_spinner);
        Spinner fgColorField = (Spinner) findViewById(R.id.fgc_spinner);
        String message = editText.getText().toString();
        SeekBar textSizer = (SeekBar) findViewById(R.id.size_seek);
        //int textSize= (Integer) spinner.getSelectedItem();
        int textSize = (Integer) textSizer.getProgress();

        String bgColor = bgColorField.getSelectedItem().toString();
        String fgColor = fgColorField.getSelectedItem().toString();

        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(TEXT_SIZE, textSize);
        intent.putExtra(BG_COLOR, bgColor);
        intent.putExtra(FG_COLOR, fgColor);
        startActivity(intent);
    }
}
